# login.py
import customtkinter
from tkinter import *
from tkinter import messagebox
from db import login

app = Tk()
app.title('Login')
app.geometry("850x700")
app.config(bg='lightblue')

font1 = ('Helvetica', 15, 'bold')

frame3 = customtkinter.CTkFrame(app,bg_color='lightblue',fg_color='white',width=590,height=500)
frame3.place(x=300,y=50)

def login_action():
    username = username_entry.get()
    password = password_entry.get()

    if username != '' and password != '':
        if login(username, password):
            messagebox.showinfo('Success', 'Login successful.')
        else:
            messagebox.showerror('Error', 'Username or password is invalid!!!')
    else:
        messagebox.showerror('Error', 'Enter all data.')


username_label = Label(frame3, font=font1, text='Username:', bg='lightblue')
username_label.pack(pady=10)
username_entry = Entry(frame3, font=font1)
username_entry.pack(pady=10)

password_label = Label(frame3, font=font1, text='Password:', bg='lightblue')
password_label.pack(pady=10)
password_entry = Entry(frame3, font=font1, show='*')
password_entry.pack(pady=10)

login_button = Button(frame3, text='Login', command=login_action, font=font1, bg='green', fg='white')
login_button.pack(pady=20)

# "Don't have an account" link
signup_link = Label(frame3, text="Don't have an account? Click here to sign up", font=('Arial', 10), fg='blue', cursor='hand2')
signup_link.pack(pady=10)

# Function to open signup window
def open_signup_window():
    app.destroy()  # Close the login window
    import signupfinal  # Import and run the signup script

signup_link.bind("<Button-1>", lambda e: open_signup_window())

app.mainloop()