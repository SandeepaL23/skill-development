# signup.py
import customtkinter
from tkinter import *
from tkinter import messagebox
from db import create_table, signup

def open_next_page(username):
    app.destroy()
    import loginfinal

app = Tk()
app.title('Sign Up')
app.geometry("850x700")
app.config(bg='lightblue')

font1 = ('Helvetica', 15, 'bold')

create_table()
frame1 = customtkinter.CTkFrame(app,bg_color='lightblue',fg_color='white',width=590,height=500)
frame1.place(x=300,y=50)

def signup_action():
    username = username_entry.get()
    password = password_entry.get()

    if username != '' and password != '':
        if signup(username, password):
            messagebox.showinfo('Success', 'Welcome,'+username)
            open_next_page(username)
        else:
            messagebox.showerror('Error', 'Username already exists.')
    else:
        messagebox.showerror('Error', 'Enter all data.')

username_label = Label(frame1, font=font1, text='Username:', bg='lightblue')
username_label.pack(pady=10)
username_entry = Entry(frame1, font=font1,fg='grey',border=3)
username_entry.pack(pady=10)

password_label = Label(frame1, font=font1, text='Password:', bg='lightblue')
password_label.pack(pady=10)
password_entry = Entry(frame1, font=font1, show='*',fg='grey',border=3)
password_entry.pack(pady=10)

signup_button = Button(frame1, text='Sign Up', command=signup_action, font=font1, bg='green', fg='white')
signup_button.pack(pady=20)

# "Already have an account" link
login_link = Label(frame1, text="Already have an account? Click here to login", font=('Arial', 10), fg='blue', cursor='hand2')
login_link.pack(pady=10)

# Function to open login window
def open_login_window():
    app.destroy()  # Close the signup window
    import loginfinal  # Import and run the login script

login_link.bind("<Button-1>", lambda e: open_login_window())

app.mainloop()